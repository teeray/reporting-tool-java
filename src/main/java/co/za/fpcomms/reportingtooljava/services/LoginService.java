//package co.za.fpcomms.reportingtooljava.services;
//
//import co.za.fpcomms.reportingtooljava.entities.CostEntity;
//import co.za.fpcomms.reportingtooljava.repositories.CostRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Service;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.persistence.Cache;
//import javax.persistence.EntityGraph;
//import javax.persistence.EntityManager;
//import javax.persistence.EntityManagerFactory;
//import javax.persistence.FlushModeType;
//import javax.persistence.Persistence;
//import javax.persistence.PersistenceUnitUtil;
//import javax.persistence.Query;
//import javax.persistence.SynchronizationType;
//import javax.persistence.criteria.CriteriaBuilder;
//import javax.persistence.metamodel.Metamodel;
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
//@Service
//public class LoginService {
//
//    @Autowired
//    CostRepository costRepository;
//
////    public String handleFileUpload(MultipartFile file) {
////
////        costRepository.deleteAll();
////
////        EntityManager em = entityManagerFactory().createEntityManager();
////        em.setFlushMode(FlushModeType.COMMIT);
////
////        int batchSize = 500;
////
////        BufferedReader br;
////        List<String> result = new ArrayList<>();
////        try {
////
////            String line;
////            InputStream is = file.getInputStream();
////            br = new BufferedReader(new InputStreamReader(is));
////
////            em.getTransaction().begin();
////
////            int id = 1;
////
////            while ((line = br.readLine()) != null) {
////                CostEntity costEntity = new CostEntity();
////
////                result.add(line);
////
////                String[] arr = line.split(",");
////
////                for (String row: arr) {
////                    row.replace("\"", "");
////                }
////
////                costEntity.setId(id);
////                costEntity.setDestination(arr[0]);
////                costEntity.setDestinationGroup(arr[1]);
////                costEntity.setDescription(arr[2]);
////                costEntity.setCountry(arr[3]);
////                costEntity.setOffPeakPeriod(arr[4]);
////                costEntity.setFirstInterval(arr[5]);
////                costEntity.setNextInterval(arr[6]);
////                costEntity.setFirstPrice(arr[7]);
////                costEntity.setNextPrice(arr[8]);
////                costEntity.setOffPeakFirstInterval(arr[9]);
////                costEntity.setOffPeakNextInterval(arr[10]);
////                costEntity.setOffPeakFirstPrice(arr[11]);
////                costEntity.setOffPeakNextPrice(arr[12]);
////                costEntity.setSecondOffPeakFirstPrice(arr[13]);
////                costEntity.setSecondOffPeakNextPrice(arr[14]);
////                costEntity.setPaybackRate(arr[15]);
////                costEntity.setForbidden(arr[16]);
////                costEntity.setHidden(arr[17]);
////                costEntity.setDiscontinued(arr[18]);
////                costEntity.setEffectiveFrom(arr[19]);
////                costEntity.setFormula(arr[20]);
////
////                System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&");
////
////                em.persist(costEntity);
////
////                if(id % batchSize == 0 && id > 0) {
////                    em.flush();
////                    em.clear();
////                }
////
////                id++;
////            }
////            em.getTransaction().commit();
////
////            return "Successful!";
////
////        } catch (IOException e) {
////            System.err.println(e.getMessage());
////            return "Failed!";
////        }
////
////    }
//
//    public Boolean handleFileUpload(MultipartFile file) {
//
//        costRepository.deleteAll();
//
//        BufferedReader br;
//        List<String> result = new ArrayList<>();
//        List<CostEntity> list = new ArrayList<>();
//        try {
//
//            String line;
//            InputStream is = file.getInputStream();
//            br = new BufferedReader(new InputStreamReader(is));
//
//            int id = 1;
//
//            while ((line = br.readLine()) != null) {
//                CostEntity costEntity = new CostEntity();
//
//                result.add(line);
//
//                String[] arr = line.split(",");
//
//                for (String row: arr) {
//                    row.replace("\"", "");
//                }
//
//                costEntity.setId(id);
//                costEntity.setDestination(arr[0]);
//                costEntity.setDestinationGroup(arr[1]);
//                costEntity.setDescription(arr[2]);
//                costEntity.setCountry(arr[3]);
//                costEntity.setOffPeakPeriod(arr[4]);
//                costEntity.setFirstInterval(arr[5]);
//                costEntity.setNextInterval(arr[6]);
//                costEntity.setFirstPrice(arr[7]);
//                costEntity.setNextPrice(arr[8]);
//                costEntity.setOffPeakFirstInterval(arr[9]);
//                costEntity.setOffPeakNextInterval(arr[10]);
//                costEntity.setOffPeakFirstPrice(arr[11]);
//                costEntity.setOffPeakNextPrice(arr[12]);
//                costEntity.setSecondOffPeakFirstPrice(arr[13]);
//                costEntity.setSecondOffPeakNextPrice(arr[14]);
//                costEntity.setPaybackRate(arr[15]);
//                costEntity.setForbidden(arr[16]);
//                costEntity.setHidden(arr[17]);
//                costEntity.setDiscontinued(arr[18]);
//                costEntity.setEffectiveFrom(arr[19]);
//                costEntity.setFormula(arr[20]);
//
//                System.out.println(id);
//
//                list.add(costEntity);
//
//                id++;
//            }
//
//            costRepository.saveAll(list);
//            System.out.println("))))))))))))))))))");
//
//            return true;
//
//        } catch (IOException e) {
//            System.err.println(e.getMessage());
//            return false;
//        }
//
//    }
//
//}
