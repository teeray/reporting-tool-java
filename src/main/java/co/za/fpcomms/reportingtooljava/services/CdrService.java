package co.za.fpcomms.reportingtooljava.services;

import co.za.fpcomms.reportingtooljava.entities.CdrBaseEntity;
//import co.za.fpcomms.reportingtooljava.entities.CdrDurationEntity;
//import co.za.fpcomms.reportingtooljava.entities.CdrEntity;
import co.za.fpcomms.reportingtooljava.repositories.CdrRepository;
import co.za.fpcomms.reportingtooljava.util.GeneralUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


@Service
@Scope("prototype")
public class CdrService {

    private CdrRepository cdrRepository;
    @Autowired
    private GeneralUtil generalUtil;
    private List<String> extList;
    private List<CdrBaseEntity> finalList;

    @Autowired
    public CdrService(CdrRepository cdrRepository){
        this.cdrRepository = cdrRepository;
    }

    public List<CdrBaseEntity> getByPinCode(String pinCode, boolean allowInternal) {
        List<CdrBaseEntity> dbList = this.cdrRepository.findByEntityPK_AccountCode(pinCode);

        dbList = filterEntities(dbList, allowInternal);

        return dbList;
    }

    public List<CdrBaseEntity> getBySrc(String src, boolean allowInternal) {
        List<CdrBaseEntity> dbList = cdrRepository.findByEntityPK_Src(src);
        dbList = filterEntities(dbList, allowInternal);

        return dbList;
    }

    public List<CdrBaseEntity> getAllCdrRecords() {
        List<CdrBaseEntity> dbList = cdrRepository.findAll();
        System.out.println(dbList.size());
        extList = new ArrayList<>();
        finalList = new ArrayList<>();

        Iterator<CdrBaseEntity> dbListItr = dbList.iterator();
        while (dbListItr.hasNext()) {
            CdrBaseEntity ext = dbListItr.next();

            this.generalUtil.filterClid(ext);
            this.generalUtil.updateCellNo(ext);
            //ext.setDurationSeconds(Integer.parseInt(ext.getDuration()));
            //System.out.println(ext.getDuration());
            addToList(ext);
        }

        Collections.sort(finalList);

        // CONVERT TIME TO HH:MM:SS
        Iterator finalListItr = finalList.iterator();

        while (finalListItr.hasNext()) {
            CdrBaseEntity ext = (CdrBaseEntity) finalListItr.next();
            this.generalUtil.updateTimeFormat(ext);
        } // END

        Collections.sort(finalList);

        return finalList;
    }

    public List<CdrBaseEntity> getTopCallsByDuration(Long howManyCalls) {
        List<CdrBaseEntity> dbList;
        dbList = cdrRepository.findAll();

        List<CdrBaseEntity> listToSort = new ArrayList<>();

        Iterator itr = dbList.iterator();

        while (itr.hasNext()) {
            CdrBaseEntity call = (CdrBaseEntity) itr.next();
            listToSort.add(call);
        }

        Collections.sort(listToSort);
        Iterator itr2 = listToSort.iterator();

        List<CdrBaseEntity> sortedTopCallsList = new ArrayList<>();

        for (int i = 0; i < howManyCalls; i++) {
            CdrBaseEntity call = (CdrBaseEntity) itr2.next();
            sortedTopCallsList.add(call);
        }

        // CONVERT TIME TO HH:MM:SS
        Iterator sortedTopCallsItr = sortedTopCallsList.iterator();

        while (sortedTopCallsItr.hasNext()) {
            CdrBaseEntity durationEntity = (CdrBaseEntity) sortedTopCallsItr.next();
            String convertedTime = this.generalUtil.convertSecondsToTime(durationEntity.getDuration());
            durationEntity.setDuration(convertedTime);

            int hours = Integer.parseInt(convertedTime.substring(0,2));
            int mins = Integer.parseInt(convertedTime.substring(3,5));

            durationEntity.setCost(String.valueOf((hours * 60) + mins));
        } // END

        return sortedTopCallsList;
    }

    //for extension summary report
    private void addToList(CdrBaseEntity ext){
        // if extension is not in list
        if (!(extList.contains(ext.getClid()))) {
            extList.add(ext.getClid());
            finalList.add(ext);
            ext.setNumOfCalls(ext.getNumOfCalls() + 1L);
        } else { //if extension is in list
            calculateAndSetSum(ext, extList.indexOf(ext.getClid()));
        }
    }

    //for extension summary report
    private void calculateAndSetSum(CdrBaseEntity ext, int index) {
        CdrBaseEntity extensionToEdit = finalList.get(index);

        int duration = Integer.parseInt(extensionToEdit.getDuration());
        int durationToAdd = Integer.parseInt(ext.getDuration());
        Long totalCalls = extensionToEdit.getNumOfCalls();

        extensionToEdit.setNumOfCalls(totalCalls + 1L);

        extensionToEdit.setDuration(Integer.toString(duration + durationToAdd));
    }

    //removes unwanted entities and updates certain properties as per business requirements
    private List<CdrBaseEntity> filterEntities(List<CdrBaseEntity> dbList, boolean allowInternal) {
        List<CdrBaseEntity> itemsToRemove = new ArrayList<>();

        Iterator itr = dbList.iterator();

        while (itr.hasNext()) {
            CdrBaseEntity ext = (CdrBaseEntity) itr.next();

            //this.generalUtil.filterClid(ext);
            this.generalUtil.updateCellNo(ext);

            if (this.generalUtil.isInternal(ext, allowInternal)) {
                itemsToRemove.add(ext);
            } else if (this.generalUtil.isInteger(ext.getDuration())) { // if duration is in seconds format
                this.generalUtil.updateTimeFormat(ext);
                //                } else { // if duration is in time format
                //                    int seconds = convertTimeUtil.convertTimeToSeconds(ext.getDuration());
                //                    String convertedTime = convertTimeUtil.convertSecondsToTime(Integer.toString(seconds));
                //                    ext.setDuration(convertedTime);
                //                }
                //            }
            } // END OF CONVERT TIME
        }
        dbList.removeAll(itemsToRemove);

        return dbList;
    }
}
