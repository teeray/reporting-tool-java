//package co.za.fpcomms.reportingtooljava.services;
//
//import co.za.fpcomms.reportingtooljava.entities.CdrEntity;
//import co.za.fpcomms.reportingtooljava.entities.CdrRepository;
//import co.za.fpcomms.reportingtooljava.util.ConvertTime;
//import co.za.fpcomms.reportingtooljava.util.Validations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.List;
//
//@Service
//public class ExtensionDetailedService {
//
//    @Autowired
//    private CdrRepository cdrRepository;
//
//    @Autowired
//    private ConvertTime convertTimeUtil;
//
//    public List<CdrEntity> getBySrc(String src, boolean allowInternal) {
//        List<CdrEntity> extList = cdrRepository.findBySrc(src);
//        List<CdrEntity> itemsToRemove = new ArrayList<>();
//        Validations validations = new Validations();
//
//        // CONVERT TIME TO HH:MM:SS
//        Iterator itr = extList.iterator();
//
//        while (itr.hasNext()) {
//            CdrEntity ext = (CdrEntity) itr.next();
//
//            validations.filterClid(ext);
//
//            if (validations.isInternal(ext, allowInternal)) {
//                itemsToRemove.add(ext);
//            } else if (isInteger(ext.getDuration())) { // if duration is in seconds format
//                    String convertedTime = convertTimeUtil.convertSecondsToTime(ext.getDuration());
//                    ext.setDuration(convertedTime);
//
//                    int hours = Integer.parseInt(convertedTime.substring(0, 2));
//                    int mins = Integer.parseInt(convertedTime.substring(3, 5));
//                    ext.setCost(String.valueOf((hours * 60) + mins));
//
//                //                } else { // if duration is in time format
//                //                    int seconds = convertTimeUtil.convertTimeToSeconds(ext.getDuration());
//                //                    String convertedTime = convertTimeUtil.convertSecondsToTime(Integer.toString(seconds));
//                //                    ext.setDuration(convertedTime);
//                //                }
//                //            }
//                } // END OF CONVERT TIME
//            }
//
//        extList.removeAll(itemsToRemove);
//
//        return extList;
//    }
//
//    private boolean isInteger(String duration) {
//        try {
//            Integer.parseInt(duration);
//            return true;
//        } catch (NumberFormatException nfe) {
//            return false;
//        }
//    }
//
//}
