//package co.za.fpcomms.reportingtooljava.entities;
//
//import javax.persistence.Column;
//import javax.persistence.Embeddable;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.Table;
//
//@Embeddable
//public class CdrDurationEntity extends CdrBaseEntity implements Comparable<CdrDurationEntity> {
//
//    @Column(name = "duration")
//    private String duration;
//
//    public String getDuration() {
//        return duration;
//    }
//
//    public void setDuration(String duration) {
//        this.duration = duration;
//    }
//
//    @Override
//    public int compareTo(CdrDurationEntity o) {
//
//        return Integer.parseInt(o.getDuration()) - Integer.parseInt(this.duration);
//    }
//}
