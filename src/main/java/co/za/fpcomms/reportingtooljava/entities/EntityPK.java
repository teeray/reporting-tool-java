package co.za.fpcomms.reportingtooljava.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class EntityPK implements Serializable, Comparable<EntityPK> {
    @Column(name = "calldate")
    private String callDate;
    @Column(name = "clid")
    private String clid;
    @Column(name = "src")
    private String src;
    @Column(name = "dcontext")
    private String dContext;
    @Column(name = "accountcode")
    private String accountCode;
    @Column(name = "uniqueid")
    private String id;
    @Column(name = "cnum")
    private String cnum;
    @Column(name = "duration")
    private String duration;
    @Column(name = "dst")
    private String destination;

    public EntityPK() {

    }

    public EntityPK(String callDate, String clid, String src, String dContext, String accountCode, String id, String cnum, String duration, String destination) {
        this.callDate = callDate;
        this.clid = clid;
        this.src = src;
        this.dContext = dContext;
        this.accountCode = accountCode;
        this.id = id;
        this.cnum = cnum;
        this.duration = duration;
        this.destination = destination;
    }

    public String getCallDate() {
        return callDate;
    }

    public String getClid() {
        return clid;
    }

    public void setClid(String clid) {
        this.clid = clid;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getdContext() {
        return dContext;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public String getId() {
        return id;
    }

    public String getCnum() {
        return cnum;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    @Override
    public int compareTo(EntityPK o) {

        return Integer.parseInt(o.getDuration()) - Integer.parseInt(o.getDuration());
    }
}
