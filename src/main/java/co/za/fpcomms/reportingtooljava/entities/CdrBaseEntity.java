package co.za.fpcomms.reportingtooljava.entities;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

@Entity
@Table(name = "cdr")
public class CdrBaseEntity implements Serializable, Comparable<CdrBaseEntity> {

    @EmbeddedId
    private EntityPK entityPK;

    @Transient
    private Long numOfCalls = 0L;

    @JsonInclude()
    @Transient
    private String cost;

    @Transient
    private int durationSeconds;

    public CdrBaseEntity() {

    }

    public CdrBaseEntity(String callDate, String clid, String src, String dContext, String accountCode, String id, String cnum, String duration, String destination) {
        this.entityPK = new EntityPK(callDate, clid, src, dContext, accountCode, id, cnum, duration, destination);
    }

    public String getCallDate() {
        return entityPK.getCallDate();
    }

    public String getClid() {
        return entityPK.getClid();
    }

    public void setClid(String clid) {
        entityPK.setClid(clid);
    }

    public String getSrc() {
        return entityPK.getSrc();
    }

    public void setSrc(String src) {
        entityPK.setSrc(src);
    }

    public String getId() {
        return entityPK.getId();
    }

    public String getdContext() {
        return entityPK.getdContext();
    }

    public String getAccountCode() {
        return entityPK.getAccountCode();
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getCnum() {
        return entityPK.getCnum();
    }

    public String getDuration() {
        return entityPK.getDuration();
    }

    public void setDuration(String duration) {
        entityPK.setDuration(duration);
    }

    public String getDestination() {
        return entityPK.getDestination();
    }

    public void setDestination(String destination) {
        entityPK.setDestination(destination);
    }

    public Long getNumOfCalls() {
        return numOfCalls;
    }

    public void setNumOfCalls(Long numOfCalls) {
        this.numOfCalls = numOfCalls;
    }

    public int getDurationSeconds() {
        return durationSeconds;
    }

    public void setDurationSeconds(int durationSeconds) {
//        String[] arr = time.split(":");
//        int hours = Integer.parseInt(arr[0]);
//        int minutes = Integer.parseInt(arr[1]);
//        int seconds = Integer.parseInt(arr[2]);
//
//        this.durationSeconds = (hours * 3600) + (minutes * 60) + (seconds);
        this.durationSeconds = durationSeconds;
    }

    @Override
    public int compareTo(CdrBaseEntity o) {
        String first = getDuration();
        int finalFirstSeconds;

        try {
            Integer.parseInt(first);
            finalFirstSeconds = Integer.parseInt(first);
        } catch (NumberFormatException nfe) {
            String[] arr = first.split(":");
            int hours = Integer.parseInt(arr[0]);
            int minutes = Integer.parseInt(arr[1]);
            int seconds = Integer.parseInt(arr[2]);

            finalFirstSeconds = (hours * 3600) + (minutes * 60) + (seconds);
        }

        String second = o.getDuration();
        int finalSecondSeconds;
        try {
            Integer.parseInt(second);
            finalSecondSeconds = Integer.parseInt(second);
        } catch (NumberFormatException nfe) {
            String[] arr2 = second.split(":");
            int hours2 = Integer.parseInt(arr2[0]);
            int minutes2 = Integer.parseInt(arr2[1]);
            int seconds2 = Integer.parseInt(arr2[2]);

            finalSecondSeconds = (hours2 * 3600) + (minutes2 * 60) + (seconds2);
        }

        if (finalFirstSeconds == finalSecondSeconds) {
            return 0;
        } else return (finalFirstSeconds < finalSecondSeconds) ? 1 : -1;
    }

}
