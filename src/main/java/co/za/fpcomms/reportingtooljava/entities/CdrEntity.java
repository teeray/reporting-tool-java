//package co.za.fpcomms.reportingtooljava.entities;
//
//import javax.persistence.Column;
//import javax.persistence.Embeddable;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import javax.persistence.Transient;
//
//@Embeddable
//public class CdrEntity extends CdrBaseEntity {
//
//    @Column(name = "dst")
//    private String destination;
//
//    @Column(name = "duration")
//    private String duration;
//
//    @Transient
//    private Long numOfCalls = 0L;
//
//    public String getDestination() {
//        return destination;
//    }
//
//    public void setDestination(String destination) {
//        this.destination = destination;
//    }
//
//    public String getDuration() {
//        return duration;
//    }
//
//    public void setDuration(String duration) {
//        this.duration = duration;
//    }
//
//    public Long getNumOfCalls() {
//        return numOfCalls;
//    }
//
//    public void setNumOfCalls(Long numOfCalls) {
//        this.numOfCalls = numOfCalls;
//    }
//}
