package co.za.fpcomms.reportingtooljava.facades.top40;

import co.za.fpcomms.reportingtooljava.entities.CdrBaseEntity;
import co.za.fpcomms.reportingtooljava.services.CdrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Top40Controller implements Top40Api {

    @Autowired
    private CdrService cdrService;

    @Override
    public List<CdrBaseEntity> sortByDuration(Long howManyCalls) {
        return this.cdrService.getTopCallsByDuration(howManyCalls);
    }
}
