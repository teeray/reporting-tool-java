//package co.za.fpcomms.reportingtooljava.facades.login;
//
//import co.za.fpcomms.reportingtooljava.services.LoginService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;
//
//@RestController
//public class FileUploadController implements FileUploadApi {
//
//    @Autowired
//    LoginService loginService;
//
//    @Override
//    public ResponseEntity<Boolean> handleFileUpload(MultipartFile file) {
//        System.out.println("test");
//
//        boolean isSuccessful = this.loginService.handleFileUpload(file);
//
//        return new ResponseEntity<>(isSuccessful, HttpStatus.OK);
//    }
//}
