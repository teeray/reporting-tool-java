package co.za.fpcomms.reportingtooljava.facades.login;

import co.za.fpcomms.reportingtooljava.models.DbUser;
import co.za.fpcomms.reportingtooljava.models.SetupDTO;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

public interface SetupDetailsApi {

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/login/updateDbDetails", method = RequestMethod.POST, consumes = {"application/json"})
    void updateDetails(@RequestBody SetupDTO setupDTO);

//    @CrossOrigin(origins = "http://localhost:4200")
//    @RequestMapping(value = "/login/setStaticData", method = RequestMethod.POST)
//    void setStaticData(@RequestBody DbUser dbUser);

}
