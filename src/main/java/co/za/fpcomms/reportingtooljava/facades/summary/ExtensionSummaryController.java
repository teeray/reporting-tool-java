package co.za.fpcomms.reportingtooljava.facades.summary;

import co.za.fpcomms.reportingtooljava.entities.CdrBaseEntity;
import co.za.fpcomms.reportingtooljava.services.CdrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Scope("prototype")
public class ExtensionSummaryController implements ExtensionSummaryApi {

    private CdrService cdrService;

    @Autowired
    public ExtensionSummaryController(CdrService cdrService){
        this.cdrService = cdrService;
    }

    @Override
    public List<CdrBaseEntity> getAllExtensions(){

        return this.cdrService.getAllCdrRecords();
    }
}