package co.za.fpcomms.reportingtooljava.facades.summary;

import co.za.fpcomms.reportingtooljava.entities.CdrBaseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

public interface ExtensionSummaryApi {

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/reports/summary")
    List<CdrBaseEntity> getAllExtensions();

}
