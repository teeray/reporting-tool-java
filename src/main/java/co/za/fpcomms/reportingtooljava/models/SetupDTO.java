package co.za.fpcomms.reportingtooljava.models;

public class SetupDTO {

    public static DbUser dbUser;
    public static NameAndPin[] nameAndPinCode;

    public DbUser getDbUser() {
        return dbUser;
    }

    public void setDbUser(DbUser dbUser) {
        SetupDTO.dbUser = dbUser;
    }

    public NameAndPin[] getNameAndPinCode() {
        return nameAndPinCode;
    }

    public void setNameAndPinCode(NameAndPin[] nameAndPinCode) {
        SetupDTO.nameAndPinCode = nameAndPinCode;
    }
}
