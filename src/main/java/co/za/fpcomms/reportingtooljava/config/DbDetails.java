package co.za.fpcomms.reportingtooljava.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Component
public class DbDetails {

    private String dbUrl = "jdbc:mysql://127.0.0.1:3306/asteriskcdrdb?verifyServerCertificate=false&useSSL=true";

    private String dbUserName = "root";

    private String dbPassword = "CallSoftvoip1234";

    public String getDbUrl() {
        return dbUrl;
    }

    public void setDbUrl(String dbUrl) {
        this.dbUrl = dbUrl;
    }

    public String getDbUserName() {
        return dbUserName;
    }

    public void setDbUserName(String dbUserName) {
        this.dbUserName = dbUserName;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }
}
