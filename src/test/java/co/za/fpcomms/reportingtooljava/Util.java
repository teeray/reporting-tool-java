package co.za.fpcomms.reportingtooljava;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.regex.Pattern;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Util {

    @Test
    public void convertTimeToSeconds() {
        String time = "02:00:45";
        String[] arr = time.split(":");
        int hours = Integer.parseInt(arr[0]);
        System.out.println(hours);
        int minutes = Integer.parseInt(arr[1]);
        System.out.println(minutes);
        int seconds = Integer.parseInt(arr[2]);
        System.out.println(seconds);
        int finalSeconds = (hours * 3600) + (minutes * 60) + (seconds);

        System.out.println(finalSeconds);
    }

}
